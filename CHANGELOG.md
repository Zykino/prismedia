# Changelog

## v 0.?

### Features
 - Add the possibility to upload thumbnail

## v0.5

### Features
 - plan your Peertube videos! Stable release
 - Support for Peertube beta4
 - More examples in NFO
 - Better support for multilines descriptions

### Fixes
 - Display datetime for output
 - plan video only if upload is successful